﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowNote : MonoBehaviour {

    public bool playing;

	// Use this for initialization
	void Start () {
        playing = false;
        SystemManager.lane = gameObject;
	}

	// Update is called once per frame
	void Update () {
        if (playing == true)
        {
            gameObject.transform.Translate(new Vector3(0, 0, SystemManager.speed * Time.deltaTime * -1.0f));
        }
	}
}
