﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JudgeMaterial : MonoBehaviour {

    public int lanenum;
    public Material offMaterial;
    public Material onMaterial;

    string[] keys = new string[5] {
        SystemManager.key0.ToString(),
        SystemManager.key1.ToString(),
        SystemManager.key2.ToString(),
        SystemManager.key3.ToString(),
        SystemManager.key4.ToString() };

    MeshRenderer meshRenderer;

    // Use this for initialization
    void Start () {
        meshRenderer = GetComponent<MeshRenderer>();

        meshRenderer.material = offMaterial;
	}

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(keys[lanenum]))
        {
            meshRenderer.material = onMaterial;
        }
        if (Input.GetKeyUp(keys[lanenum]))
        {
            meshRenderer.material = offMaterial;
        }
	}
}
