﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadInstance : MonoBehaviour {

    public GameObject noteSinglePref;
    public GameObject noteLongPref;
    public GameObject noteLongStartPref;
    public GameObject noteLongEndPref;
    public GameObject lane;

    float sperb;
    float mperb;

    // Use this for initialization
    void Start ()
    {
        bool bLongflg = false;
        sperb = (float)(60.0 / SystemManager.bpm[SystemManager.selectsongindexnum]);
        mperb = sperb * SystemManager.speed;
        SystemManager.sperb = sperb;
        SystemManager.mperb = mperb;
        List<float>[] liNoteTiming = new List<float>[5];
        List<bool>[] liLongNote = new List<bool>[5];
        for(int i = 0; i < 5; ++i)
        {
            liNoteTiming[i] = (List<float>)SystemManager.timing.GetValue(SystemManager.selectsongindexnum, i);
            liLongNote[i] = (List<bool>)SystemManager.longnote.GetValue(SystemManager.selectsongindexnum, i);
        }
        SystemManager.arrnote = new List<List<GameObject>>();
        SystemManager.arrlong = new List<List<GameObject>>();
        for(int i = 0; i < 5; ++i)
        {
            SystemManager.arrnote.Add(new List<GameObject>());
            SystemManager.arrlong.Add(new List<GameObject>());
            for (int j = 0; j < liNoteTiming[i].Count; ++j)
            {
                float fTiming = liNoteTiming[i][j];

                if (liLongNote[i][j])
                {
                    bLongflg = true;
                    SystemManager.arrnote[i].Add(Instantiate(noteLongStartPref,
                            new Vector3(i * 3, 0,
                            (4 * fTiming + (SystemManager.offset[SystemManager.selectsongindexnum] * 0.001f
                            + SystemManager.sysoffset) / sperb) * mperb),
                            Quaternion.identity,
                            lane.transform));
                }
                else
                {
                    if (bLongflg)
                    {
                        bLongflg = false;
                        SystemManager.arrnote[i].Add(Instantiate(noteLongEndPref,
                            new Vector3(i * 3, 0,
                            (4 * fTiming + (SystemManager.offset[SystemManager.selectsongindexnum] * 0.001f
                            + SystemManager.sysoffset) / sperb) * mperb),
                            Quaternion.identity,
                            lane.transform));
                        SystemManager.arrlong[i].Add(Instantiate(noteLongPref,
                            new Vector3(0, 0, 0), Quaternion.identity, lane.transform));
                        SystemManager.arrlong[i][SystemManager.arrlong[i].Count - 1].GetComponent<LineRenderer>().SetPosition(
                            0, SystemManager.arrnote[i][SystemManager.arrnote[i].Count - 2].transform.position);
                        SystemManager.arrlong[i][SystemManager.arrlong[i].Count - 1].GetComponent<LineRenderer>().SetPosition(
                            1, SystemManager.arrnote[i][SystemManager.arrnote[i].Count - 1].transform.position);
                        //SystemManager.arrlong[i][SystemManager.arrlong[i].Count - 1]
                    }
                    else
                    {
                        SystemManager.arrnote[i].Add(Instantiate(noteSinglePref,
                            new Vector3(i * 3, 0,
                            (4 * fTiming + (SystemManager.offset[SystemManager.selectsongindexnum] * 0.001f
                            + SystemManager.sysoffset) / sperb) * mperb),
                            Quaternion.identity,
                            lane.transform));
                    }
                }
            }
        }
	}

	// Update is called once per frame
	void Update () {

	}
}
