﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScore : MonoBehaviour {

    public GameObject noteSinglePref;
    public GameObject noteLongPref;
    public GameObject noteLongStartPref;
    public GameObject noteLongEndPref;
    public GameObject lane;

    // Use this for initialization
    void Start () {
        SystemManager.speed = 50.0f;
        LoadConfig();
        CreateNote();
        SceneManager.LoadScene("Scenes/Play");
	}

	// Update is called once per frame
	void Update () {

	}

    public void LoadConfig()
    {
        SystemManager.score = (new StreamReader(Application.dataPath + "/Resources/TestMusic/score.string", System.Text.Encoding.Default)).ReadToEnd();
        SystemManager.config = (new StreamReader(Application.dataPath + "/Resources/TestMusic/config.string", System.Text.Encoding.Default)).ReadToEnd();
        int start = SystemManager.config.IndexOf("BPM:") + "BPM:".Length;
        string bpm_str = SystemManager.config.Substring(start);
        SystemManager.bpm[0] = int.Parse(bpm_str);
    }


    public void CreateNote()
    {
        float note_pos = 0.0f;
        float note_time = 0.0f;
        StringReader clone = new StringReader(SystemManager.score);

        int note_pos_num = 0;
        int note_time_num = 0;
        bool long_note = false;

        while (true)
        {
            int dummy = clone.Read();
            if (dummy == '2')
            {
                if (long_note == false)
                {
                    note_pos_num += 2;
                    note_time_num++;
                    long_note = true;
                }
                else
                {
                    note_pos_num++;
                    note_time_num++;
                    long_note = false;
                }
            }
            else if (dummy == '1')
            {
                note_pos_num++;
                note_time_num++;
            }
            else if (dummy == -1)
            {
                break;
            }
        }

        SystemManager.notes = new GameObject[note_pos_num];
        SystemManager.notes_time = new float[note_time_num];
        note_pos_num = 0;
        note_time_num = 0;
        clone = new StringReader(SystemManager.score);

        while(clone.Peek() != -1)
        {
            string line = clone.ReadLine();
            StringReader score_line = new StringReader(line);
            int len = line.Length;
            float sperb = (float)(60.0 / (SystemManager.bpm[0] * len * 0.25));
            float mperb = SystemManager.speed * sperb;
            for ( int j = 0; j < line.Length; ++j)
            {
                switch (score_line.Read())
                {
                    case '0':
                        note_pos += mperb;
                        break;
                    case '1':
                        SystemManager.notes[note_pos_num] = Instantiate<GameObject>(noteSinglePref, new Vector3(0, 0, note_pos), Quaternion.identity, lane.transform);
                        SystemManager.notes_time[note_time_num] = note_time;
                        note_time += sperb;
                        note_pos += mperb;
                        note_time_num++;
                        note_pos_num++;
                        break;
                    case '2':
                        if(long_note == false)
                        {
                            SystemManager.notes[note_pos_num] = Instantiate<GameObject>(noteLongStartPref, new Vector3(0, 0, note_pos), Quaternion.identity, lane.transform);
                            SystemManager.notes_time[note_time_num] = note_time;
                            note_pos_num++;
                            SystemManager.notes[note_pos_num] = Instantiate<GameObject>(noteLongPref, new Vector3(0, 0, note_pos), Quaternion.identity, lane.transform);
                            note_time += sperb;
                            note_pos += mperb;
                            note_pos_num++;
                            note_time_num++;
                            long_note = true;
                        }
                        else
                        {
                            float mir = note_pos - SystemManager.notes[note_pos_num - 1].transform.localPosition.z;
                            SystemManager.notes[note_pos_num - 1].transform.localScale = new Vector3(1,1,mir);
                            SystemManager.notes[note_pos_num - 1].transform.Translate(new Vector3(0, 0, mir * 0.5f));
                            SystemManager.notes[note_pos_num] = Instantiate<GameObject>(noteLongEndPref, new Vector3(0, 0, note_pos), Quaternion.identity, lane.transform);
                            SystemManager.notes_time[note_time_num] = note_time;
                            note_time += sperb;
                            note_pos += mperb;
                            note_time_num++;
                            note_pos_num++;
                            long_note = false;
                        }
                        break;
                    default:
                        Debug.Log("def");
                        break;
                }
            }
        }
    }

}
