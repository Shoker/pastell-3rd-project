﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectManager : MonoBehaviour {

    public GameObject goCamera;
    public AudioSource[] asJudgeSound;
    public AudioSource asSoundPlayer;

    private bool judgedGreat = false;
    private bool judgedGood = false;
    private bool judgedBad = false;
    private bool judgedMiss = false;

    int[] nNoteStartNum = new int[5] { 0, 0, 0, 0, 0 };
    int[] nNoteEndNum = new int[5] { 0, 0, 0, 0, 0 };
    int nPlaySoundNum = 0;
    bool[] bLongLock = new bool[5] { false, false, false, false, false };
    string[] keys = new string[5] {
        SystemManager.key0.ToString(),
        SystemManager.key1.ToString(),
        SystemManager.key2.ToString(),
        SystemManager.key3.ToString(),
        SystemManager.key4.ToString() };
    bool opening = true;
    bool ending = false;
    bool openingLocked = false;
    bool endingLocked = false;
    List<bool>[] liLongNote;

	// Use this for initialization
	void Start () {
        SystemManager.combo = 0;
        SystemManager.point = 0;
        SystemManager.num_great = 0;
        SystemManager.num_good = 0;
        SystemManager.num_bad = 0;
        liLongNote = new List<bool>[5];
        for (int i = 0; i < 5; ++i)
        {
            liLongNote[i] = (List<bool>)SystemManager.longnote.GetValue(SystemManager.selectsongindexnum, i);
        }
    }
    private void LateUpdate()
    {
        judgedBad = false;
        judgedGood = false;
        judgedGreat = false;
        judgedMiss = false;
    }
    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(SystemManager.keyx.ToString()))
        {
            SceneManager.LoadScene("Scenes/Select");
        }
        if (opening)
        {
            if (openingLocked == false)
            {
                StartCoroutine("StopOpening");
                openingLocked = true;
            }
        }
        else if(opening | ending == false)
        {
            for(int i = 0; i < 5; ++i)
            {
                if (bLongLock[i])
                {
                    if (Input.GetKeyUp(keys[i]))
                    {
                        asJudgeSound[nPlaySoundNum].Play();
                        nPlaySoundNum = (nPlaySoundNum + 1) & 15;
                        bLongLock[i] = false;
                        JudgeLongEndNote(i);
                    }
                }
            }
            for (int i = 0; i < 5; ++i)
            {
                if (Input.GetKeyDown(keys[i]))
                {
                    asJudgeSound[nPlaySoundNum].Play();
                    nPlaySoundNum = (nPlaySoundNum + 1) & 15;
                    JudgeNote(i);
                }
            }
            for(int i = 0; i < SystemManager.arrnote.Count; i++)
            {
                if(SystemManager.arrnote[i].Count == nNoteStartNum[i])
                {
                    continue;
                }
                if (SystemManager.arrnote[i][nNoteStartNum[i]].transform.position.z
                    < goCamera.transform.position.z  + SystemManager.myoffset - SystemManager.mperb *SystemManager.judgeBadprop)
                {
                    if (!bLongLock[i])
                    {
                        //asJudgeSound[nPlaySoundNum].Play();
                        //nPlaySoundNum = (nPlaySoundNum + 1) & 15;
                        //SystemManager.arrnote[i][nNoteStartNum[i]].SetActive(false);
                        if (liLongNote[i][nNoteStartNum[i]])
                        {
                            Destroy(SystemManager.arrnote[i][nNoteStartNum[i]]);
                            Destroy(SystemManager.arrnote[i][nNoteStartNum[i] + 1]);
                            nNoteStartNum[i] += 2;
                            SystemManager.num_bad += 2;
                            Destroy(SystemManager.arrlong[i][nNoteEndNum[i]]);
                            nNoteEndNum[i]++;
                            Debug.Log("deleted note");
                            Debug.Log("deleted note");
                            SystemManager.combo = 0;
                            judgedMiss = true;
                        }
                        else
                        {
                            Destroy(SystemManager.arrnote[i][nNoteStartNum[i]]);
                            nNoteStartNum[i]++;
                            SystemManager.num_bad += 1;
                            Debug.Log("deleted note");
                            SystemManager.combo = 0;
                            judgedMiss = true;
                        }
                    }
                    else
                    {
                        bLongLock[i] = false;
                        Destroy(SystemManager.arrlong[i][nNoteEndNum[i]]);
                        nNoteEndNum[i]++;
                        Destroy(SystemManager.arrnote[i][nNoteStartNum[i]]);
                        nNoteStartNum[i]++;
                        SystemManager.num_bad += 1;
                        Debug.Log("deleted note");
                        SystemManager.combo = 0;
                        judgedMiss = true;
                    }
                }
            }
            goCamera.transform.Translate(0, 0, SystemManager.speed * Time.deltaTime);

            if (!asSoundPlayer.isPlaying)
            {
                ending = true;
            }

        }
        else if (ending)
        {
            if (endingLocked == false)
            {
                StartCoroutine("StopEnding");
                endingLocked = true;
            }
        }
	}

    void OnDestroy()
    {
        Destroy(GameObject.Find("lane"));
        for (int i = 0; i < 5; ++i)
        {
            SystemManager.arrnote[0].Clear();
            SystemManager.arrlong[0].Clear();
        }
        SystemManager.arrnote.Clear();
        SystemManager.arrlong.Clear();

        for (int i = 0; i < 3; ++i)
        {
            if (SystemManager.point > SystemManager.savedata[SystemManager.selectsongindexnum].score[i])
            {
                SystemManager.savedata[SystemManager.selectsongindexnum].score[2] = SystemManager.point;
                for (int j = 2; j > i; --j)
                {
                    int tmp = SystemManager.savedata[SystemManager.selectsongindexnum].score[j - 1];
                    SystemManager.savedata[SystemManager.selectsongindexnum].score[j - 1]
                        = SystemManager.savedata[SystemManager.selectsongindexnum].score[j];
                    SystemManager.savedata[SystemManager.selectsongindexnum].score[j] = tmp;
                }
                break;
            }
        }
    }

    void JudgeNote(int nLaneIndexNum)
    {
        if (SystemManager.arrnote[nLaneIndexNum].Count <= nNoteStartNum[nLaneIndexNum])
        {
            return;
        }
        float fNotePos = SystemManager.arrnote[nLaneIndexNum][nNoteStartNum[nLaneIndexNum]].transform.position.z;
        if ( Mathf.Abs(fNotePos - goCamera.transform.position.z - SystemManager.myoffset) < SystemManager.mperb * SystemManager.judgeBadprop)
        {
            if(Mathf.Abs(fNotePos - goCamera.transform.position.z - SystemManager.myoffset) < SystemManager.mperb * SystemManager.judgeGreatprop)
            {
                LongNoteLock(nLaneIndexNum);
                Destroy(SystemManager.arrnote[nLaneIndexNum][nNoteStartNum[nLaneIndexNum]]);
                nNoteStartNum[nLaneIndexNum]++;
                SystemManager.num_great += 1;
                Debug.Log("Great");
                SystemManager.combo += 1;
                SystemManager.point += 500;
                judgedGreat = true;
            }
            else
            {
                if (Mathf.Abs(fNotePos - goCamera.transform.position.z - SystemManager.myoffset) < SystemManager.mperb * SystemManager.judgeGoodprop)
                {
                    LongNoteLock(nLaneIndexNum);
                    Destroy(SystemManager.arrnote[nLaneIndexNum][nNoteStartNum[nLaneIndexNum]]);
                    nNoteStartNum[nLaneIndexNum]++;
                    SystemManager.num_good += 1;
                    Debug.Log("Good");
                    SystemManager.combo += 1;
                    SystemManager.point += 200;
                    judgedGood = true;
                }
                else
                {
                    LongNoteLock(nLaneIndexNum);
                    Destroy(SystemManager.arrnote[nLaneIndexNum][nNoteStartNum[nLaneIndexNum]]);
                    nNoteStartNum[nLaneIndexNum]++;
                    SystemManager.num_bad += 1;
                    Debug.Log("Bad");
                    SystemManager.combo += 1;
                    SystemManager.point += 100;
                    judgedBad = true;
                }
            }
        }
    }

    void LongNoteLock(int nLaneIndexNum)
    {
        if (liLongNote[nLaneIndexNum][nNoteStartNum[nLaneIndexNum]])
        {
            bLongLock[nLaneIndexNum] = true;
        }
    }

    void JudgeLongEndNote(int nLaneIndexNum)
    {
        if (SystemManager.arrnote[nLaneIndexNum].Count <= nNoteStartNum[nLaneIndexNum])
        {
            return;
        }
        float fNotePos = SystemManager.arrnote[nLaneIndexNum][nNoteStartNum[nLaneIndexNum]].transform.position.z;
        if (Mathf.Abs(fNotePos - goCamera.transform.position.z - SystemManager.myoffset) < SystemManager.mperb * SystemManager.judgeBadprop)
        {
            if (Mathf.Abs(fNotePos - goCamera.transform.position.z - SystemManager.myoffset) < SystemManager.mperb * SystemManager.judgeGreatprop)
            {
                Destroy(SystemManager.arrlong[nLaneIndexNum][nNoteEndNum[nLaneIndexNum]]);
                nNoteEndNum[nLaneIndexNum]++;
                Destroy(SystemManager.arrnote[nLaneIndexNum][nNoteStartNum[nLaneIndexNum]]);
                nNoteStartNum[nLaneIndexNum]++;
                SystemManager.num_great += 1;
                Debug.Log("Great");
                SystemManager.combo += 1;
                SystemManager.point += 500;
                judgedGreat = true;
            }
            else
            {
                if (Mathf.Abs(fNotePos - goCamera.transform.position.z - SystemManager.myoffset) < SystemManager.mperb * SystemManager.judgeGoodprop)
                {
                    Destroy(SystemManager.arrlong[nLaneIndexNum][nNoteEndNum[nLaneIndexNum]]);
                    nNoteEndNum[nLaneIndexNum]++;
                    Destroy(SystemManager.arrnote[nLaneIndexNum][nNoteStartNum[nLaneIndexNum]]);
                    nNoteStartNum[nLaneIndexNum]++;
                    SystemManager.num_good += 1;
                    Debug.Log("Good");
                    SystemManager.combo += 1;
                    SystemManager.point += 200;
                    judgedGood = true;
                }
                else
                {
                    Destroy(SystemManager.arrlong[nLaneIndexNum][nNoteEndNum[nLaneIndexNum]]);
                    nNoteEndNum[nLaneIndexNum]++;
                    Destroy(SystemManager.arrnote[nLaneIndexNum][nNoteStartNum[nLaneIndexNum]]);
                    nNoteStartNum[nLaneIndexNum]++;
                    SystemManager.num_bad += 1;
                    Debug.Log("Bad");
                    SystemManager.combo += 1;
                    SystemManager.point += 100;
                    judgedBad = true;
                }
            }
        }
        else
        {
            Destroy(SystemManager.arrlong[nLaneIndexNum][nNoteEndNum[nLaneIndexNum]]);
            nNoteEndNum[nLaneIndexNum]++;
            Destroy(SystemManager.arrnote[nLaneIndexNum][nNoteStartNum[nLaneIndexNum]]);
            nNoteStartNum[nLaneIndexNum]++;
            SystemManager.num_bad += 1;
            Debug.Log("Bad");
            SystemManager.combo = 0;
            judgedBad = true;
        }
    }

    public bool GetJudgedGreat()
    {
        return judgedGreat;
    }

    public bool GetJudgedGood()
    {
        return judgedGood;
    }

    public bool GetJudgedBad()
    {
        return judgedBad;
    }

    public bool GetJudgedMiss()
    {
        return judgedMiss;
    }
    public IEnumerator StopOpening()
    {
        for (int i = 0; i < 60; ++i)
        {
            yield return null;
        }
        asSoundPlayer.Play();
        opening = false;
    }
    public IEnumerator StopEnding()
    {
        for (int i = 0; i < 60; ++i)
        {
            yield return null;
        }
        SceneManager.LoadScene("Scenes/Result");
    }
}
