﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using LitJson;

public class ResultManager : MonoBehaviour {

    string[] keys = new string[5] {
        SystemManager.key0.ToString(),
        SystemManager.key1.ToString(),
        SystemManager.key2.ToString(),
        SystemManager.key3.ToString(),
        SystemManager.key4.ToString() };

    void Awake()
    {
        SystemManager.savedata[SystemManager.selectsongindexnum].playernum += 1;
        SystemManager.playcounter += 1;
        //write playcounter
        using (StreamWriter sw = new StreamWriter(Application.dataPath + "/playcounter.txt"))
        {
            sw.WriteLine(SystemManager.playcounter.ToString());
        }
    }

    // Use this for initialization
    void Start () {

    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(keys[2]))
        {
            if(SystemManager.svfFileName[SystemManager.selectsongindexnum] == null)
            {
                SystemManager.svfFileName[SystemManager.selectsongindexnum]
                    = Application.dataPath + "/Musicdata/"
                    + SystemManager.folderName[SystemManager.selectsongindexnum]
                    + "/" + "data.svf";
            }
            //encrypt
            string json = JsonMapper.ToJson(SystemManager.savedata[SystemManager.selectsongindexnum]);
            string iv;
            string base64;
            byte[] src = Encoding.UTF8.GetBytes(json);
            byte[] dst;
            string PasswordChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int count = 16;
            StringBuilder sb = new StringBuilder(count);
            for (int i = count - 1; i >= 0; i--)
            {
                char c = PasswordChars[UnityEngine.Random.Range(0, PasswordChars.Length)];
                sb.Append(c);
            }
            iv = sb.ToString();
            dst = null;
            using (RijndaelManaged rijndael = new RijndaelManaged())
            {
                rijndael.Padding = PaddingMode.PKCS7;
                rijndael.Mode = CipherMode.CBC;
                rijndael.KeySize = 256;
                rijndael.BlockSize = 128;

                byte[] key = Encoding.UTF8.GetBytes("pastel3rdproject");
                byte[] vec = Encoding.UTF8.GetBytes(iv);

                using (ICryptoTransform encryptor = rijndael.CreateEncryptor(key, vec))
                using (MemoryStream ms = new MemoryStream())
                using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                {
                    cs.Write(src, 0, src.Length);
                    cs.FlushFinalBlock();
                    dst = ms.ToArray();
                }
            }
            base64 = Convert.ToBase64String(dst);

            //save
            byte[] ivBytes = Encoding.UTF8.GetBytes(iv);
            byte[] base64Bytes = Encoding.UTF8.GetBytes(base64);
            using (FileStream fs = new FileStream(SystemManager.svfFileName[SystemManager.selectsongindexnum],
                FileMode.Create, FileAccess.Write))
            using (BinaryWriter bw = new BinaryWriter(fs))
            {
                bw.Write(ivBytes.Length);
                bw.Write(ivBytes);

                bw.Write(base64Bytes.Length);
                bw.Write(base64Bytes);
            }
            SceneManager.LoadScene("Scenes/Select");
        }
	}
}
