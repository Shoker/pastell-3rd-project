﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAudio : MonoBehaviour {

    public AudioSource acMusicPlayer;

	// Use this for initialization
	void Start () {
        acMusicPlayer.clip = SystemManager.sound[SystemManager.selectsongindexnum];
	}

	// Update is called once per frame
	void Update () {

	}
}
