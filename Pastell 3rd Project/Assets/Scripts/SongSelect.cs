﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SongSelect : MonoBehaviour {

    public GameObject objParentCanvas;
    public GameObject prefabSong;
    public AudioSource asSoundPlayer;
    public Text_Update textUpdater;

    public Font font;

    GameObject[] objSong;
    TextMesh[] textMeshes;
    const int nActiveSongNum = 5;
    int nUpperEnd = 0;
    int nDownerEnd = 0;
    const float nMoveDistance = 1.0f;
    int nCurrentSongNum = 0;
    int nLoopCount = 0;
    char cPushedButton = 'F';
    bool bPushedButton = false;

    string[] keys = new string[5] {
        SystemManager.key0.ToString(),
        SystemManager.key1.ToString(),
        SystemManager.key2.ToString(),
        SystemManager.key3.ToString(),
        SystemManager.key4.ToString() };


    // Use this for initialization
    void Start () {
        nCurrentSongNum = SystemManager.selectsongindexnum;
        objSong = new GameObject[SystemManager.soundNum];
        textMeshes = new TextMesh[SystemManager.soundNum];
        nUpperEnd = (int)(nActiveSongNum / 2.0) + 1 + SystemManager.selectsongindexnum < SystemManager.soundNum
            ? (int)(nActiveSongNum / 2.0) + 1 + SystemManager.selectsongindexnum
            : (int)(nActiveSongNum / 2.0) + 1 + SystemManager.selectsongindexnum - SystemManager.soundNum;
        nDownerEnd = SystemManager.selectsongindexnum - (int)(nActiveSongNum / 2.0) - 1 >= 0
            ? SystemManager.selectsongindexnum - (int)(nActiveSongNum / 2.0) - 1
            : SystemManager.soundNum  - 1 + SystemManager.selectsongindexnum - (int)(nActiveSongNum / 2.0);
        for(int i = 0; i < SystemManager.soundNum; ++i)
        {
            objSong[i] = Instantiate(prefabSong, objParentCanvas.transform);
            objSong[i].GetComponentInChildren<TextMesh>().text = SystemManager.title[i];
            textMeshes[i] = objSong[i].GetComponent<TextMesh>();
            GUIStyle style = new GUIStyle();
            style.font = font;
            Vector2 vec2 = style.CalcSize(
                new GUIContent(SystemManager.title[i]));
            if (vec2.x > 200)
            {
                textMeshes[i].fontSize = (int)(20 * Mathf.Exp((200 - vec2.x) / 50.0f) + 20);
            }
            else
            {
                textMeshes[i].fontSize = 40;
            }
        }
        for (int i = 0; i < (int)(nActiveSongNum / 2.0) + 1; ++i)
        {
            int indexnum = 0;
            if(i + SystemManager.selectsongindexnum < SystemManager.soundNum)
            {
                indexnum = i + SystemManager.selectsongindexnum;
            }
            else
            {
                indexnum = i + SystemManager.selectsongindexnum - SystemManager.soundNum;
            }
            objSong[indexnum].transform.localPosition = new Vector3(0, i * nMoveDistance, 0);
        }
        for(int i = 1; i < (int)(nActiveSongNum / 2.0) + 1; ++i)
        {
            int indexnnum = 0;
            if(SystemManager.selectsongindexnum - i >= 0)
            {
                indexnnum = SystemManager.selectsongindexnum - i;
            }
            else
            {
                indexnnum = SystemManager.soundNum + SystemManager.selectsongindexnum - i;
            }
            objSong[indexnnum].transform.localPosition = new Vector3(0, -i * nMoveDistance, 0);
        }
        if (nUpperEnd < nDownerEnd)
        {
            for (int i = nUpperEnd; i <= nDownerEnd; ++i)
            {
                objSong[i].SetActive(false);
                textMeshes[i].color = new Color(0, 0, 0, 0);
            }
        }
        else
        {
            for(int i = nUpperEnd; i < SystemManager.soundNum; ++i)
            {
                objSong[i].SetActive(false);
                textMeshes[i].color = new Color(0, 0, 0, 0);
            }
            for(int i = nDownerEnd; i >= 0; --i)
            {
                objSong[i].SetActive(false);
                textMeshes[i].color = new Color(0, 0, 0, 0);
            }
        }
        asSoundPlayer.clip = SystemManager.sound[SystemManager.selectsongindexnum];
        asSoundPlayer.Play();
    }

	// Update is called once per frame
	void Update () {
        if (bPushedButton)
        {
            if (cPushedButton == SystemManager.key1)
            {
                if (nLoopCount < 20)
                {
                    if (nUpperEnd > nDownerEnd)
                    {
                        for (int k = nDownerEnd; k < nUpperEnd; ++k)
                        {
                            objSong[k].transform.Translate(0, nMoveDistance * 0.05f, 0);
                        }
                    }
                    else
                    {
                        for (int k = 0; k < nUpperEnd; ++k)
                        {
                            objSong[k].transform.Translate(0, nMoveDistance * 0.05f, 0);
                        }
                        for (int k = SystemManager.soundNum - 1; k >= nDownerEnd; --k)
                        {
                            objSong[k].transform.Translate(0, nMoveDistance * 0.05f, 0);
                        }
                    }

                    asSoundPlayer.volume = 1.0f - nLoopCount * 0.05f;

                    textMeshes[(nUpperEnd == 0) ? SystemManager.soundNum - 1 : nUpperEnd - 1].color
                        = new Color(0, 0, 0, 255 * Mathf.Pow((20 - nLoopCount) * 0.05f, 4));
                    textMeshes[nDownerEnd].color = new Color(0, 0, 0, 255 * Mathf.Pow(nLoopCount * 0.05f, 4));

                    nLoopCount++;
                }
                else
                {
                    bPushedButton = false;
                    objSong[(nUpperEnd == 0) ? SystemManager.soundNum - 1 : nUpperEnd - 1].SetActive(false);
                    nUpperEnd = (nUpperEnd == 0) ? SystemManager.soundNum - 1 : nUpperEnd - 1;
                    nDownerEnd = (nDownerEnd == 0) ? SystemManager.soundNum - 1 : nDownerEnd - 1;
                    asSoundPlayer.Stop();
                    asSoundPlayer.volume = 1.0f;
                    asSoundPlayer.clip = SystemManager.sound[nCurrentSongNum];
                    asSoundPlayer.Play();
                }
            }
            else if (cPushedButton == SystemManager.key3)
            {
                if (nLoopCount < 20)
                {
                    if (nUpperEnd > nDownerEnd)
                    {
                        for (int k = nDownerEnd + 1; k <= nUpperEnd; ++k)
                        {
                            objSong[k].transform.Translate(0, -nMoveDistance * 0.05f, 0);
                        }
                    }
                    else
                    {
                        for (int k = 0; k <= nUpperEnd; ++k)
                        {
                            objSong[k].transform.Translate(0, -nMoveDistance * 0.05f, 0);
                        }
                        for (int k = SystemManager.soundNum - 1; k > nDownerEnd; --k)
                        {
                            objSong[k].transform.Translate(0, -nMoveDistance * 0.05f, 0);
                        }
                    }
                    asSoundPlayer.volume = 1.0f - nLoopCount / 20.0f;

                    textMeshes[(nDownerEnd + 1 == SystemManager.soundNum) ? 0 : nDownerEnd + 1].color
                        = new Color(0, 0, 0, 255 * Mathf.Pow((20 - nLoopCount) * 0.05f, 4));
                    textMeshes[nUpperEnd].color = new Color(0, 0, 0, 255 * Mathf.Pow(nLoopCount * 0.05f, 4));

                    nLoopCount++;
                }
                else
                {
                    bPushedButton = false;
                    objSong[(nDownerEnd + 1 == SystemManager.soundNum) ? 0 : nDownerEnd + 1].SetActive(false);
                    nDownerEnd = (nDownerEnd + 1 == SystemManager.soundNum) ? 0 : nDownerEnd + 1;
                    nUpperEnd = (nUpperEnd + 1 == SystemManager.soundNum) ? 0 : nUpperEnd + 1;
                    asSoundPlayer.Stop();
                    asSoundPlayer.volume = 1.0f;
                    asSoundPlayer.clip = SystemManager.sound[nCurrentSongNum];
                    asSoundPlayer.Play();
                }
            }
        }
        else
        {
            if (Input.GetKey(keys[1]))
            {
                bPushedButton = true;
                cPushedButton = SystemManager.key1;
                nCurrentSongNum = (nCurrentSongNum == 0) ? SystemManager.soundNum - 1 : nCurrentSongNum - 1;
                SystemManager.selectsongindexnum = nCurrentSongNum;
                nLoopCount = 0;
                textUpdater.StartMethod();
                objSong[nDownerEnd].transform.localPosition = new Vector3(0, nMoveDistance * -(int)((nActiveSongNum / 2.0) + 1), 0);
                objSong[nDownerEnd].SetActive(true);
            }
            else if (Input.GetKey(keys[3]))
            {
                bPushedButton = true;
                cPushedButton = SystemManager.key3;
                nCurrentSongNum = (nCurrentSongNum == SystemManager.soundNum - 1) ? 0 : nCurrentSongNum + 1;
                SystemManager.selectsongindexnum = nCurrentSongNum;
                nLoopCount = 0;
                textUpdater.StartMethod();
                objSong[nUpperEnd].transform.localPosition = new Vector3(0, nMoveDistance * (int)((nActiveSongNum / 2.0) + 1), 0);
                objSong[nUpperEnd].SetActive(true);
            }
            else if (Input.GetKey(keys[0]))
            {

            }
            else if (Input.GetKey(keys[4]))
            {

            }
            else if (Input.GetKey(keys[2]))
            {
                SystemManager.selectsongindexnum = nCurrentSongNum;
                SceneManager.LoadScene("Scenes/Play");
            }
        }
	}
}
