﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSelector : MonoBehaviour {

    public Sprite[] sprBackground;
    public Sprite[] sprEnemy;

    public GameObject objBackground;
    public GameObject objHero;
    public GameObject[] objEnemy;
    public Animator[] aniEnemy;
    public RuntimeAnimatorController[] runtimeAnimatorControllers1;
    public RuntimeAnimatorController[] runtimeAnimatorControllers2;

    int nStageIndexNum;

	// Use this for initialization
	void Start () {
        nStageIndexNum = Random.Range(0, sprBackground.Length);
        objBackground.GetComponent<SpriteRenderer>().sprite = sprBackground[nStageIndexNum];
        for(int i = 0; i < objEnemy.Length; ++i)
        {
            objEnemy[i].GetComponent<SpriteRenderer>().sprite = sprEnemy[nStageIndexNum];
            if (nStageIndexNum == 0)
                aniEnemy[i].runtimeAnimatorController = runtimeAnimatorControllers1[i];
            else if (nStageIndexNum == 1)
                aniEnemy[i].runtimeAnimatorController = runtimeAnimatorControllers2[i];
        }
	}

	// Update is called once per frame
	void Update () {

	}
}
