﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using LitJson;

public class SystemInitialize : MonoBehaviour {

    public Texture2D noImage;
    public LineRenderer lineRenderer;
    public GameObject nowloading;
    public GameObject completed;

    enum LoadLevel
    {
        Sound,
        Jacket,
        Text,
        Savedata
    };
    private class LoadProgressParameter
    {
        public int nIsDoneNum;
        public float fCurrProgress;
        public bool bLoadStart;
        public bool bLoadFinished;
        public LoadProgressParameter()
        {
            nIsDoneNum = 0;
            fCurrProgress = 0f;
            bLoadStart = false;
            bLoadFinished = false;
        }
    };
    private LoadProgressParameter[] loadProg = new LoadProgressParameter[4];
    private string[] sCoroutineMethodName = { "LoadSound", "LoadJacket", "LoadText", "LoadSavedata" };
    private int nLoadCount = 0;

    // Use this for initialization
    void Start() {
        Debug.Log(Application.platform);
        //set parameter
        Application.targetFrameRate = 60;
        SystemManager.speed = 100.0f;
        SystemManager.sysoffset = 0.0f;
        SystemManager.selectsongindexnum = 0;
        //find files
        string[] subFolders = Directory.GetDirectories(Application.dataPath + "/MusicData", "*", SearchOption.AllDirectories);
        SystemManager.folderName = new string[subFolders.Length];
        SystemManager.txtFileName = new string[subFolders.Length];
        SystemManager.wavFileName = new string[subFolders.Length];
        SystemManager.svfFileName = new string[subFolders.Length];
        SystemManager.pngFileName = new string[subFolders.Length];
        for (int i = 0; i < subFolders.Length; ++i) {
            SystemManager.folderName[i] = Path.GetFileNameWithoutExtension(subFolders[i]);
            try
            {
                string[] fileName = Directory.GetFiles(subFolders[i], "*.txt", SearchOption.AllDirectories);
                if (fileName.Length > 1)
                {
                    Debug.LogWarning("txt file is much than one");
                }
                SystemManager.txtFileName[i] = fileName[0];
            }
            catch
            {
                Debug.LogError("txt file is not found in " + subFolders[i]);
            }
            try
            {
                string[] musicName = Directory.GetFiles(subFolders[i], "*.wav", SearchOption.AllDirectories);
                if (musicName.Length > 1)
                {
                    Debug.LogWarning("wav file is much than one");
                }
                SystemManager.wavFileName[i] = musicName[0];
            }
            catch
            {
                Debug.LogError("wav file is not found in " + subFolders[i]);
            }
            try
            {
                string[] pngName = Directory.GetFiles(subFolders[i], "*.png", SearchOption.AllDirectories);
                if(pngName.Length > 1)
                {
                    Debug.Log("png file is much than one");
                }
                SystemManager.pngFileName[i] = pngName[0];
            }
            catch
            {
                Debug.Log("png file is not found in " + subFolders[i]);
                SystemManager.pngFileName[i] = null;
            }
            try
            {
                string[] svfName = Directory.GetFiles(subFolders[i], "*.svf", SearchOption.TopDirectoryOnly);
                if(svfName.Length > 1)
                {
                    Debug.LogWarning("svf file is much than one");
                }
                SystemManager.svfFileName[i] = svfName[0];
            }
            catch
            {
                SystemManager.svfFileName[i] = null;
            }
        }
        SystemManager.soundNum = subFolders.Length;

        //Initialize loading parameter
        for (int i = 0; i < 4; ++i) loadProg[i] = new LoadProgressParameter();
        lineRenderer.SetPosition(1, new Vector3(-8, -3, 0));

        //load playcounter
        using (StreamReader sr = new StreamReader(Application.dataPath + "/playcounter.txt"))
        {
            SystemManager.playcounter = int.Parse(sr.ReadToEnd());
        }
	}

	// Update is called once per frame
	void Update () {
        if(!loadProg[nLoadCount].bLoadStart)
        {
            StartCoroutine(sCoroutineMethodName[nLoadCount]);
            loadProg[nLoadCount].bLoadStart = true;
        }
        else
        {
            if (loadProg[nLoadCount].bLoadFinished) nLoadCount += 1;
            if (nLoadCount == 4)
            {
                nowloading.SetActive(false);
                completed.SetActive(true);
                SceneManager.LoadScene("Scenes/Select");
            }
            else if(nLoadCount < 4)
            {
                lineRenderer.SetPosition(1, new Vector3(
                    -8.0f + (4.0f * (nLoadCount + ((loadProg[nLoadCount].nIsDoneNum + loadProg[nLoadCount].fCurrProgress) / SystemManager.soundNum))),
                    -3.0f,
                    0f));
            }
        }
    }

    //Load Sound
    public IEnumerator LoadSound()
    {
        int loadprogindexnum = (int)LoadLevel.Sound;
        if (SystemManager.soundNum <= 0)
        {
            Debug.LogWarning("wav file is nothing");
        }

        SystemManager.sound = new AudioClip[SystemManager.soundNum];

        for (int i = 0; i < SystemManager.soundNum; ++i)
        {
            AudioClip wav = null;
            WWW www = new WWW("file:///" + SystemManager.wavFileName[i]);
            while (!www.isDone)
            {
                loadProg[loadprogindexnum].fCurrProgress = www.progress;
                yield return null;
            }
            loadProg[loadprogindexnum].fCurrProgress = 0;
            loadProg[loadprogindexnum].nIsDoneNum += 1;
            wav = www.GetAudioClip(false, false, AudioType.WAV);
            if (wav == null) Debug.Log("wav == null : "
                + SystemManager.folderName[i] + "/" + SystemManager.wavFileName[i]);
            SystemManager.sound[i] = wav;
            yield return true;
        }
        loadProg[loadprogindexnum].bLoadFinished = true;
    }

    public IEnumerator LoadJacket()
    {
        int loadprogindexnum = (int)LoadLevel.Jacket;

        if (SystemManager.soundNum <= 0)
        {
            Debug.LogWarning("png file is nothing");
        }

        SystemManager.jackets = new Texture2D[SystemManager.soundNum];

        for (int i = 0; i < SystemManager.soundNum; ++i)
        {
            Texture2D texture = null;
            if (File.Exists(SystemManager.pngFileName[i])) {

                WWW www = new WWW("file:///" + SystemManager.pngFileName[i]);
                while (!www.isDone)
                {
                    loadProg[loadprogindexnum].fCurrProgress = www.progress;
                    yield return null;
                }
                loadProg[loadprogindexnum].fCurrProgress = 0;
                loadProg[loadprogindexnum].nIsDoneNum += 1;
                texture = www.texture;
            }
            else
            {
                texture = noImage;
            }
            if (texture.width != 256 || texture.height != 256)
            {
                TextureScale.Bilinear(texture, 256, 256);
            }
            SystemManager.jackets[i] = texture;
            yield return true;
        }
        loadProg[loadprogindexnum].bLoadFinished = true;
    }

    public IEnumerator LoadText()
    {
        int loadprogindexnum = (int)LoadLevel.Text;

        SystemManager.title = new string[SystemManager.soundNum];
        SystemManager.lyricist = new string[SystemManager.soundNum];
        SystemManager.composer = new string[SystemManager.soundNum];
        SystemManager.song = new string[SystemManager.soundNum];
        SystemManager.bpm = new float[SystemManager.soundNum];
        SystemManager.offset = new int[SystemManager.soundNum];
        SystemManager.level = new int[SystemManager.soundNum];
        SystemManager.timing = System.Array.CreateInstance(typeof(List<float>), SystemManager.soundNum, 5);
        SystemManager.longnote = System.Array.CreateInstance(typeof(List<bool>), SystemManager.soundNum, 5);
        for (int i = 0; i < SystemManager.soundNum; ++i)
        {
            if (SystemManager.txtFileName[i] != null)
            {
                byte[] bs = System.IO.File.ReadAllBytes(SystemManager.txtFileName[i]);
                System.Text.Encoding enc = GetCode(bs);

                if(Application.platform == RuntimePlatform.WindowsPlayer && enc == null)
                {
                    Debug.LogError("type of charactor code is possibility of Shift-JIS.\n"
                        + "error file path : " + SystemManager.txtFileName[i]);
                }
                Debug.Log(SystemManager.txtFileName[i]);
                StreamReader srData = new StreamReader(SystemManager.txtFileName[i],enc);
                List<float>[] timing = new List<float>[5];
                List<bool>[] longtone = new List<bool>[5];
                for (int j = 0; j < 5; j++)
                {
                    timing[j] = new List<float>(0);
                    longtone[j] = new List<bool>(0);
                }
                string data = null;
                while ((data = srData.ReadLine()) != null)
                {
                    if (data.IndexOf("#Title") == 0)
                    {
                        string str = data.Substring("#Title".Length);
                        SystemManager.title[i] = str;
                    }
                    else if (data.IndexOf("#Lyricist") == 0)
                    {
                        string str = data.Substring("#Lyricist".Length);
                        SystemManager.lyricist[i] = str;
                    }
                    else if (data.IndexOf("#Composer") == 0)
                    {
                        string str = data.Substring("#Composer".Length);
                        SystemManager.composer[i] = str;
                    }
                    else if (data.IndexOf("#Song") == 0)
                    {
                        string str = data.Substring("#Song".Length);
                        SystemManager.song[i] = str;
                    }
                    else if (data.IndexOf("#Bpm") == 0)
                    {
                        string str = data.Substring("#Bpm".Length);
                        SystemManager.bpm[i] = float.Parse(str);
                    }
                    else if (data.IndexOf("#Offset") == 0)
                    {
                        string str = data.Substring("#Offset".Length);
                        SystemManager.offset[i] = int.Parse(str);
                    }
                    else if (data.IndexOf("#Level") == 0)
                    {
                        string str = data.Substring("#Level".Length);
                        SystemManager.level[i] = int.Parse(str);
                    }
                    else if(data.IndexOf("#Measure") == 0)
                    {
                        string str = data.Substring("#Measure".Length);

                    }
                    else if (data.IndexOf("#0") == 0)
                    {
                        string[] spliter = data.Split(':');
                        int nMeasureNum = int.Parse(spliter[0].Substring(3));
                        StringReader srMsr = new StringReader(spliter[1]);
                        int nIndexNum = 0;
                        for (int j = 0; j < spliter[2].Length; ++j)
                        {
                            int nLaneNum = spliter[2].ToCharArray()[j];
                            switch (nLaneNum)
                            {
                                case '1':
                                    nLaneNum = 0;
                                    break;
                                case '2':
                                    nLaneNum = 1;
                                    break;
                                case '3':
                                    nLaneNum = 2;
                                    break;
                                case '4':
                                    nLaneNum = 3;
                                    break;
                                case '5':
                                    nLaneNum = 4;
                                    break;
                                default:
                                    Debug.LogError("Lane is not exist.");
                                    break;
                            }
                            int nNoteType = 0;
                            do
                            {
                                nIndexNum += 1;
                            } while ((nNoteType = srMsr.Read()) == '0');
                            switch (nNoteType)
                            {
                                case '2':
                                    timing[nLaneNum].Add(nMeasureNum + (nIndexNum - 1) / (float)spliter[1].Length);
                                    longtone[nLaneNum].Add(false);

                                    for (int k = timing[nLaneNum].Count - 1; k > 0; --k)
                                    {
                                        if (k == 0)
                                        {
                                            break;
                                        }
                                        if (timing[nLaneNum][k] < timing[nLaneNum][k - 1])
                                        {
                                            float tmp = timing[nLaneNum][k];
                                            bool b = longtone[nLaneNum][k];
                                            timing[nLaneNum][k] = timing[nLaneNum][k - 1];
                                            longtone[nLaneNum][k] = longtone[nLaneNum][k - 1];
                                            timing[nLaneNum][k - 1] = tmp;
                                            longtone[nLaneNum][k - 1] = b;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    break;
                                case '4':
                                    timing[nLaneNum].Add(nMeasureNum + (nIndexNum - 1) / (float)spliter[1].Length);
                                    longtone[nLaneNum].Add(true);
                                    for (int k = timing[nLaneNum].Count - 1; k > 0; --k)
                                    {
                                        if (k == 0)
                                        {
                                            break;
                                        }
                                        if (timing[nLaneNum][k] < timing[nLaneNum][k - 1])
                                        {
                                            float tmp = timing[nLaneNum][k];
                                            bool b = longtone[nLaneNum][k];
                                            timing[nLaneNum][k] = timing[nLaneNum][k - 1];
                                            longtone[nLaneNum][k] = longtone[nLaneNum][k - 1];
                                            timing[nLaneNum][k - 1] = tmp;
                                            longtone[nLaneNum][k - 1] = b;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    break;
                                default:
                                    Debug.LogError("Note is different pattern.");
                                    break;
                            }
                        }
                    }
                }
                for (int j = 0; j < 5; j++)
                {
                    SystemManager.timing.SetValue(timing[j], i, j);
                    SystemManager.longnote.SetValue(longtone[j], i, j);
                }
            }
            else
            {
                Debug.LogError("not txt file");
            }
            loadProg[loadprogindexnum].nIsDoneNum += 1;
            yield return true;
        }
        loadProg[loadprogindexnum].bLoadFinished = true;
    }

    public IEnumerator LoadSavedata()
    {
        int loadprogindexnum = (int)LoadLevel.Savedata;

        SystemManager.savedata = new SystemManager.SaveData[SystemManager.soundNum];
        for (int i = 0; i < SystemManager.soundNum; ++i)
        {
            SystemManager.savedata[i] = new SystemManager.SaveData();
            if (SystemManager.svfFileName[i] == null)
            {
                SystemManager.savedata[i].score = new int[3] { 0, 0, 0 };
                SystemManager.savedata[i].playernum = 0;
            }
            else
            {
                //read
                byte[] ivBytes = null;
                byte[] base64Bytes = null;
                using (FileStream fs = new FileStream(SystemManager.svfFileName[i], FileMode.Open, FileAccess.Read))
                using (BinaryReader br = new BinaryReader(fs))
                {
                    int length = br.ReadInt32();
                    ivBytes = br.ReadBytes(length);

                    length = br.ReadInt32();
                    base64Bytes = br.ReadBytes(length);
                }

                string json;
                string iv = Encoding.UTF8.GetString(ivBytes);
                string base64 = Encoding.UTF8.GetString(base64Bytes);
                byte[] src = Convert.FromBase64String(base64);
                byte[] dst;
                dst = new byte[src.Length];
                using (RijndaelManaged rijndael = new RijndaelManaged())
                {
                    rijndael.Padding = PaddingMode.PKCS7;
                    rijndael.Mode = CipherMode.CBC;
                    rijndael.KeySize = 256;
                    rijndael.BlockSize = 128;

                    byte[] key = Encoding.UTF8.GetBytes("pastel3rdproject");
                    byte[] vec = Encoding.UTF8.GetBytes(iv);

                    using (ICryptoTransform decryptor = rijndael.CreateDecryptor(key, vec))
                    using (MemoryStream ms = new MemoryStream(src))
                    using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        cs.Read(dst, 0, dst.Length);
                    }
                }
                json = Encoding.UTF8.GetString(dst).Trim('\0');

                SystemManager.savedata[i] = JsonMapper.ToObject<SystemManager.SaveData>(json);
            }
            loadProg[loadprogindexnum].nIsDoneNum += 1;
            yield return true;
        }
        loadProg[loadprogindexnum].bLoadFinished = true;
    }

    /// <summary>
    /// 文字コードを判別する
    /// </summary>
    /// <remarks>
    /// Jcode.pmのgetcodeメソッドを移植したものです。
    /// Jcode.pm(http://openlab.ring.gr.jp/Jcode/index-j.html)
    /// Jcode.pmの著作権情報
    /// Copyright 1999-2005 Dan Kogai <dankogai@dan.co.jp>
    /// This library is free software; you can redistribute it and/or modify it
    ///  under the same terms as Perl itself.
    /// </remarks>
    /// <param name="bytes">文字コードを調べるデータ</param>
    /// <returns>適当と思われるEncodingオブジェクト。
    /// 判断できなかった時はnull。</returns>
    public static System.Text.Encoding GetCode(byte[] bytes)
    {
        const byte bEscape = 0x1B;
        const byte bAt = 0x40;
        const byte bDollar = 0x24;
        const byte bAnd = 0x26;
        const byte bOpen = 0x28;    //'('
        const byte bB = 0x42;
        const byte bD = 0x44;
        const byte bJ = 0x4A;
        const byte bI = 0x49;

        int len = bytes.Length;
        byte b1, b2, b3, b4;

        //Encode::is_utf8 は無視

        bool isBinary = false;
        for (int i = 0; i < len; i++)
        {
            b1 = bytes[i];
            if (b1 <= 0x06 || b1 == 0x7F || b1 == 0xFF)
            {
                //'binary'
                isBinary = true;
                if (b1 == 0x00 && i < len - 1 && bytes[i + 1] <= 0x7F)
                {
                    //smells like raw unicode
                    return System.Text.Encoding.Unicode;
                }
            }
        }
        if (isBinary)
        {
            return null;
        }

        //not Japanese
        bool notJapanese = true;
        for (int i = 0; i < len; i++)
        {
            b1 = bytes[i];
            if (b1 == bEscape || 0x80 <= b1)
            {
                notJapanese = false;
                break;
            }
        }
        if (notJapanese)
        {
            return System.Text.Encoding.ASCII;
        }

        for (int i = 0; i < len - 2; i++)
        {
            b1 = bytes[i];
            b2 = bytes[i + 1];
            b3 = bytes[i + 2];

            if (b1 == bEscape)
            {
                if (b2 == bDollar && b3 == bAt)
                {
                    //JIS_0208 1978
                    //JIS
                    return System.Text.Encoding.GetEncoding(50220);
                }
                else if (b2 == bDollar && b3 == bB)
                {
                    //JIS_0208 1983
                    //JIS
                    return System.Text.Encoding.GetEncoding(50220);
                }
                else if (b2 == bOpen && (b3 == bB || b3 == bJ))
                {
                    //JIS_ASC
                    //JIS
                    return System.Text.Encoding.GetEncoding(50220);
                }
                else if (b2 == bOpen && b3 == bI)
                {
                    //JIS_KANA
                    //JIS
                    return System.Text.Encoding.GetEncoding(50220);
                }
                if (i < len - 3)
                {
                    b4 = bytes[i + 3];
                    if (b2 == bDollar && b3 == bOpen && b4 == bD)
                    {
                        //JIS_0212
                        //JIS
                        return System.Text.Encoding.GetEncoding(50220);
                    }
                    if (i < len - 5 &&
                        b2 == bAnd && b3 == bAt && b4 == bEscape &&
                        bytes[i + 4] == bDollar && bytes[i + 5] == bB)
                    {
                        //JIS_0208 1990
                        //JIS
                        return System.Text.Encoding.GetEncoding(50220);
                    }
                }
            }
        }

        //should be euc|sjis|utf8
        //use of (?:) by Hiroki Ohzaki <ohzaki@iod.ricoh.co.jp>
        int sjis = 0;
        int euc = 0;
        int utf8 = 0;
        for (int i = 0; i < len - 1; i++)
        {
            b1 = bytes[i];
            b2 = bytes[i + 1];
            if (((0x81 <= b1 && b1 <= 0x9F) || (0xE0 <= b1 && b1 <= 0xFC)) &&
                ((0x40 <= b2 && b2 <= 0x7E) || (0x80 <= b2 && b2 <= 0xFC)))
            {
                //SJIS_C
                sjis += 2;
                i++;
            }
        }
        for (int i = 0; i < len - 1; i++)
        {
            b1 = bytes[i];
            b2 = bytes[i + 1];
            if (((0xA1 <= b1 && b1 <= 0xFE) && (0xA1 <= b2 && b2 <= 0xFE)) ||
                (b1 == 0x8E && (0xA1 <= b2 && b2 <= 0xDF)))
            {
                //EUC_C
                //EUC_KANA
                euc += 2;
                i++;
            }
            else if (i < len - 2)
            {
                b3 = bytes[i + 2];
                if (b1 == 0x8F && (0xA1 <= b2 && b2 <= 0xFE) &&
                    (0xA1 <= b3 && b3 <= 0xFE))
                {
                    //EUC_0212
                    euc += 3;
                    i += 2;
                }
            }
        }
        for (int i = 0; i < len - 1; i++)
        {
            b1 = bytes[i];
            b2 = bytes[i + 1];
            if ((0xC0 <= b1 && b1 <= 0xDF) && (0x80 <= b2 && b2 <= 0xBF))
            {
                //UTF8
                utf8 += 2;
                i++;
            }
            else if (i < len - 2)
            {
                b3 = bytes[i + 2];
                if ((0xE0 <= b1 && b1 <= 0xEF) && (0x80 <= b2 && b2 <= 0xBF) &&
                    (0x80 <= b3 && b3 <= 0xBF))
                {
                    //UTF8
                    utf8 += 3;
                    i += 2;
                }
            }
        }
        //M. Takahashi's suggestion
        //utf8 += utf8 / 2;

        System.Diagnostics.Debug.WriteLine(
            string.Format("sjis = {0}, euc = {1}, utf8 = {2}", sjis, euc, utf8));
        if (euc > sjis && euc > utf8)
        {
            //EUC
            return System.Text.Encoding.GetEncoding(51932);
        }
        else if (sjis > euc && sjis > utf8)
        {
            //SJIS
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                return System.Text.Encoding.GetEncoding(932);
            }
            else if (Application.platform == RuntimePlatform.WindowsPlayer)
            {
                Debug.LogError("Windows Player is not compatible with Shift-JIS");
                return null;
            }
        }
        else if (utf8 > euc && utf8 > sjis)
        {
            //UTF8
            return System.Text.Encoding.UTF8;
        }

        return null;
    }


    /// <summary>
    ///
    /// </summary>
    public class TextureScale
    {
        public class ThreadData
        {
            public int start;
            public int end;
            public ThreadData(int s, int e)
            {
                start = s;
                end = e;
            }
        }

        private static Color[] texColors;
        private static Color[] newColors;
        private static int w;
        private static float ratioX;
        private static float ratioY;
        private static int w2;
        private static int finishCount;
        private static Mutex mutex;

        public static void Point(Texture2D tex, int newWidth, int newHeight)
        {
            ThreadedScale(tex, newWidth, newHeight, false);
        }

        public static void Bilinear(Texture2D tex, int newWidth, int newHeight)
        {
            ThreadedScale(tex, newWidth, newHeight, true);
        }

        private static void ThreadedScale(Texture2D tex, int newWidth, int newHeight, bool useBilinear)
        {
            texColors = tex.GetPixels();
            newColors = new Color[newWidth * newHeight];
            if (useBilinear)
            {
                ratioX = 1.0f / ((float)newWidth / (tex.width - 1));
                ratioY = 1.0f / ((float)newHeight / (tex.height - 1));
            }
            else
            {
                ratioX = ((float)tex.width) / newWidth;
                ratioY = ((float)tex.height) / newHeight;
            }
            w = tex.width;
            w2 = newWidth;
            var cores = Mathf.Min(SystemInfo.processorCount, newHeight);
            var slice = newHeight / cores;

            finishCount = 0;
            if (mutex == null)
            {
                mutex = new Mutex(false);
            }
            if (cores > 1)
            {
                int i = 0;
                ThreadData threadData;
                for (i = 0; i < cores - 1; i++)
                {
                    threadData = new ThreadData(slice * i, slice * (i + 1));
                    ParameterizedThreadStart ts = useBilinear ? new ParameterizedThreadStart(BilinearScale) : new ParameterizedThreadStart(PointScale);
                    Thread thread = new Thread(ts);
                    thread.Start(threadData);
                }
                threadData = new ThreadData(slice * i, newHeight);
                if (useBilinear)
                {
                    BilinearScale(threadData);
                }
                else
                {
                    PointScale(threadData);
                }
                while (finishCount < cores)
                {
                    Thread.Sleep(1);
                }
            }
            else
            {
                ThreadData threadData = new ThreadData(0, newHeight);
                if (useBilinear)
                {
                    BilinearScale(threadData);
                }
                else
                {
                    PointScale(threadData);
                }
            }

            tex.Resize(newWidth, newHeight);
            tex.SetPixels(newColors);
            tex.Apply();

            texColors = null;
            newColors = null;
        }

        public static void BilinearScale(System.Object obj)
        {
            ThreadData threadData = (ThreadData)obj;
            for (var y = threadData.start; y < threadData.end; y++)
            {
                int yFloor = (int)Mathf.Floor(y * ratioY);
                var y1 = yFloor * w;
                var y2 = (yFloor + 1) * w;
                var yw = y * w2;

                for (var x = 0; x < w2; x++)
                {
                    int xFloor = (int)Mathf.Floor(x * ratioX);
                    var xLerp = x * ratioX - xFloor;
                    newColors[yw + x] = ColorLerpUnclamped(ColorLerpUnclamped(texColors[y1 + xFloor], texColors[y1 + xFloor + 1], xLerp),
                                                           ColorLerpUnclamped(texColors[y2 + xFloor], texColors[y2 + xFloor + 1], xLerp),
                                                           y * ratioY - yFloor);
                }
            }

            mutex.WaitOne();
            finishCount++;
            mutex.ReleaseMutex();
        }

        public static void PointScale(System.Object obj)
        {
            ThreadData threadData = (ThreadData)obj;
            for (var y = threadData.start; y < threadData.end; y++)
            {
                var thisY = (int)(ratioY * y) * w;
                var yw = y * w2;
                for (var x = 0; x < w2; x++)
                {
                    newColors[yw + x] = texColors[(int)(thisY + ratioX * x)];
                }
            }

            mutex.WaitOne();
            finishCount++;
            mutex.ReleaseMutex();
        }

        private static Color ColorLerpUnclamped(Color c1, Color c2, float value)
        {
            return new Color(c1.r + (c2.r - c1.r) * value,
                              c1.g + (c2.g - c1.g) * value,
                              c1.b + (c2.b - c1.b) * value,
                              c1.a + (c2.a - c1.a) * value);
        }
    }
}
