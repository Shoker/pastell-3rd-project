﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SystemManager{

    //KeyMap
    public static char key0 = 'c';
    public static char key1 = 'v';
    public static char key2 = 'b';
    public static char key3 = 'n';
    public static char key4 = 'm';
    public static char keyx = 'x';
    //URI
    public static string[] folderName;
    public static string[] txtFileName;
    public static string[] wavFileName;
    public static string[] svfFileName;
    public static string[] pngFileName;
    //Number
    public static int soundNum;
    //Sound
    public static AudioClip[] sound;
    //Sound information
    public static string[] title;
    public static string[] lyricist;
    public static string[] composer;
    public static string[] song;
    public static float[] bpm;
    public static int[] offset;
    public static int[] level;
    public static Texture2D[] jackets;
    //Notes Timing
    public static System.Array timing;
    public static System.Array longnote;
    //Select song information
    public static int selectsongindexnum;
    //GameObjects
    public static List<List<GameObject>> arrnote;
    public static List<List<GameObject>> arrlong;
    //Instanciate Parameter
    public static float speed;
    public static float mperb;
    public static float sperb;
    //Offset
    public static float sysoffset;
    public const float myoffset = 10f;
    //Judge Parameter
    public const float judgeGreatprop = 0.125f;
    public const float judgeGoodprop = 0.25f;
    public const float judgeBadprop = 0.50f;
    //Result Parameter
    public static int combo;
    public static int point;
    public static int num_good;
    public static int num_great;
    public static int num_bad;

    //before loading system variable
    public static string score;
    public static string config;
    public static float[] notes_time;
    public static GameObject lane;
    public static GameObject[] notes;

    //Savedata
    public class SaveData
    {
        public int[] score;
        public int playernum;
    }
    public static SaveData[] savedata;
    public static int playcounter;
}

