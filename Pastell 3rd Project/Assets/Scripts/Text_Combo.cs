﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Text_Combo : MonoBehaviour {

    TextMesh text;

	// Use this for initialization
	void Start () {
        text = GetComponent<TextMesh>();
        text.text = "コンボ\n0";
	}

	// Update is called once per frame
	void Update () {
        text.text = "コンボ\n" + SystemManager.combo.ToString();
	}
}
