﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Text_Composer : MonoBehaviour {

    TextMesh composer;

    // Use this for initialization
    void Start()
    {
        composer = GetComponent<TextMesh>();
        composer.text = "作曲" + SystemManager.composer[SystemManager.selectsongindexnum];
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator ChangeComposer()
    {
        for (int i = 1; i <= 10; ++i)
        {
            composer.color = new Color(0, 0, 0, 255 * (1 - i * 0.1f));
            yield return null;
        }
        composer.text = "作曲" + SystemManager.composer[SystemManager.selectsongindexnum];
        for (int i = 1; i <= 10; ++i)
        {
            composer.color = new Color(0, 0, 0, 255 * (i * 0.1f));
            yield return null;
        }
    }
}
