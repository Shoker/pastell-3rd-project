﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Text_Judge : MonoBehaviour {

    public ObjectManager objectManager;

    TextMesh textMesh;

	// Use this for initialization
	void Start () {
        textMesh = GetComponent<TextMesh>();
        textMesh.text = "";
	}

	// Update is called once per frame
	void Update () {
        if (objectManager.GetJudgedGreat())
        {
            textMesh.text = "Great";
        }
        if (objectManager.GetJudgedGood())
        {
            textMesh.text = "Good";
        }
        if (objectManager.GetJudgedBad())
        {
            textMesh.text = "Bad";
        }
        if (objectManager.GetJudgedMiss())
        {
            textMesh.text = "Miss";
        }
	}
}
