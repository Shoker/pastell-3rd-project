﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Text_Level : MonoBehaviour {

    TextMesh textMesh;

	// Use this for initialization
	void Start () {
        textMesh = GetComponent<TextMesh>();
        textMesh.text = "レベル" + SystemManager.level[SystemManager.selectsongindexnum].ToString();

    }

    // Update is called once per frame
    void Update () {

	}

    public IEnumerator ChangeLevel()
    {
        for (int i = 1; i <= 10; ++i)
        {
            textMesh.color = new Color(0, 0, 0, 255 * (1 - i * 0.1f));
            yield return null;
        }
        textMesh.text = "レベル" + SystemManager.level[SystemManager.selectsongindexnum].ToString();
        for (int i = 1; i <= 10; ++i)
        {
            textMesh.color = new Color(0, 0, 0, 255 * (i * 0.1f));
            yield return null;
        }
    }
}
