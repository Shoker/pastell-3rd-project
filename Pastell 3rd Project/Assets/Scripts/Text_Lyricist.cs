﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Text_Lyricist : MonoBehaviour {

    TextMesh lyricist;

    // Use this for initialization
    void Start()
    {
        lyricist = GetComponent<TextMesh>();
        lyricist.text = "作詞" + SystemManager.lyricist[SystemManager.selectsongindexnum];
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator ChangeLyricist()
    {
        for (int i = 1; i <= 10; ++i)
        {
            lyricist.color = new Color(0, 0, 0, 255 * (1 - i * 0.1f));
            yield return null;
        }
        lyricist.text = "作詞" + SystemManager.lyricist[SystemManager.selectsongindexnum];
        for (int i = 1; i <= 10; ++i)
        {
            lyricist.color = new Color(0, 0, 0, 255 * (i * 0.1f));
            yield return null;
        }
    }
}
