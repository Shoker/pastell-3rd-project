﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Text_PlayCounter : MonoBehaviour {

    TextMesh textMesh;

	// Use this for initialization
	void Start () {
        textMesh = GetComponent<TextMesh>();
        textMesh.text = "Lv."+ (SystemManager.playcounter - 1).ToString()
            + "  →  Lv." + SystemManager.playcounter.ToString();
	}

	// Update is called once per frame
	void Update () {

	}
}
