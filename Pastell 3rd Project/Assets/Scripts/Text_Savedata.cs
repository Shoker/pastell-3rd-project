﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Text_Savedata : MonoBehaviour {

    public int nScoreIndexNum;

    TextMesh score;

    // Use this for initialization
    void Start()
    {
        score = GetComponent<TextMesh>();
        score.text = (nScoreIndexNum + 1).ToString() + ":"
            + SystemManager.savedata[SystemManager.selectsongindexnum]
            .score[nScoreIndexNum].ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator ChangeScore()
    {
        for (int i = 1; i <= 10; ++i)
        {
            score.color = new Color(0, 0, 0, 255 * (1 - i * 0.1f));
            yield return null;
        }
        score.text = (nScoreIndexNum + 1).ToString() + ":"
            + SystemManager.savedata[SystemManager.selectsongindexnum]
            .score[nScoreIndexNum].ToString();
        for (int i = 1; i <= 10; ++i)
        {
            score.color = new Color(0, 0, 0, 255 * (i * 0.1f));
            yield return null;
        }
    }
}
