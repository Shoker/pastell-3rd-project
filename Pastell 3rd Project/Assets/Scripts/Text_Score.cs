﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Text_Score : MonoBehaviour {

    TextMesh text;

	// Use this for initialization
	void Start () {
        text = GetComponent<TextMesh>();
        text.text = "スコア\n0";
	}

	// Update is called once per frame
	void Update () {
        text.text = "スコア\n" + SystemManager.point.ToString();
	}
}
