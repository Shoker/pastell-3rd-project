﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Text_Title : MonoBehaviour {

    public Font font;
    TextMesh title;

	// Use this for initialization
	void Start () {
        title = GetComponent<TextMesh>();
        string str = SystemManager.title[SystemManager.selectsongindexnum];

        title.text = str;
        GUIStyle style = new GUIStyle();
        style.font = font;
        Vector2 vec2 = style.CalcSize(
            new GUIContent(SystemManager.title[SystemManager.selectsongindexnum]));
        if (vec2.x > 160)
        {
            title.fontSize = (int)(60 * Mathf.Exp((160 - vec2.x) / 50.0f) + 20);
        }
        else
        {
            title.fontSize = 80;
        }
    }

	// Update is called once per frame
	void Update () {

	}

    public IEnumerator ChangeTitle()
    {
        for (int i = 1; i <= 10; ++i)
        {
            title.color = new Color(0,0, 0, 255 * (1 - i * 0.1f));
            yield return null;
        }
        string str = SystemManager.title[SystemManager.selectsongindexnum];

        title.text = str;
        GUIStyle style = new GUIStyle();
        style.font = font;
        Vector2 vec2 = style.CalcSize(
            new GUIContent(SystemManager.title[SystemManager.selectsongindexnum]));
        if(vec2.x > 160)
        {
            title.fontSize = (int)(40 * Mathf.Exp((160 - vec2.x) / 50.0f) + 40);
        }
        else
        {
            title.fontSize = 80;
        }
        //Debug.Log(vec2.x);
        for (int i = 1; i <= 10; ++i)
        {
            title.color = new Color(0, 0, 0, 255 * (i * 0.1f));
            yield return null;
        }
    }
}
