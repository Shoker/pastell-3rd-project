﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Text_Update : MonoBehaviour{

    public Texture_Jacket jacket;
    public Text_Title title;
    public Text_Composer composer;
    public Text_Lyricist lyricist;
    public Text_Savedata savedatas;
    public Text_Level level;

    public void StartMethod()
    {
        jacket.StartCoroutine("ChangeJacket");
        title.StartCoroutine("ChangeTitle");
        composer.StartCoroutine("ChangeComposer");
        lyricist.StartCoroutine("ChangeLyricist");
        savedatas.StartCoroutine("ChangeScore");
        level.StartCoroutine("ChangeLevel");
    }
}
