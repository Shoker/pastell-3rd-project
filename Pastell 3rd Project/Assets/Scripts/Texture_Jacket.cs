﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class Texture_Jacket : MonoBehaviour {

    SpriteRenderer spriteRenderer;
    Sprite[] sprite;

	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        sprite = new Sprite[SystemManager.soundNum];
        for(int i = 0; i < SystemManager.soundNum; ++i)
        {
            sprite[i] = Sprite.Create(SystemManager.jackets[i],
                new Rect(0, 0, 256, 256),
                Vector2.zero);
        }
        spriteRenderer.sprite = sprite[SystemManager.selectsongindexnum];
	}

	// Update is called once per frame
	void Update () {

	}

    public IEnumerator ChangeJacket()
    {
        for (int i = 1; i <= 10; ++i) {
            spriteRenderer.color = new Color(255,255,255,255 * (1 - i * 0.1f));
            yield return null;
        }
        spriteRenderer.sprite = sprite[SystemManager.selectsongindexnum];
        for(int i = 1; i <= 10; ++i)
        {
            spriteRenderer.color = new Color(255, 255, 255, 255 * (i * 0.1f));
            yield return null;
        }
    }



}
