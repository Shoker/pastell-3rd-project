﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeGauge : MonoBehaviour {

    public AudioSource audioSource;

    LineRenderer lineRenderer;
	// Use this for initialization
	void Start () {
        lineRenderer = GetComponent<LineRenderer>();
	}

	// Update is called once per frame
	void Update () {
        if (audioSource.isPlaying)
        {
            float timeprop = audioSource.timeSamples / (float)audioSource.clip.samples;
            lineRenderer.SetPosition(1, new Vector3(0, 0, 3 - 3 * timeprop));
        }
	}
}
